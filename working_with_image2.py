# First installing the pillow library in terminal
# pip install pillow
import PIL.Image
from PIL import Image
# Extracting the file and displaying the images.
mask = Image.open("/Users/sabitamanandhar/Desktop/pythonProject/working with images with python/mask.png")
wordmatrix = Image.open("/Users/sabitamanandhar/Desktop/pythonProject/working with images with python/word_matrix.png")
mask.show()
wordmatrix.show()

# Checking the size
print(mask.size)
print(wordmatrix.size)
# Overlapping the two images
# But not possible because both are different sizes
# So resizing the mask
re_mask = mask.resize((1015, 559))
print(re_mask.size)

# Now both images sizes are equal and now adding transparency
maskalpha = re_mask.putalpha(200)
maskalpha.show()

image_paste = wordmatrix.paste(re_mask)
image_paste.show()

# blending / overlapping can also be done through:
alphablend = Image.blend(re_mask, wordmatrix, alpha=.3)
alphablend.show()