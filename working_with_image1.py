# First installing the pillow library in terminal
# pip install pillow
import PIL.Image
from PIL import Image
# Getting and then displaying the image using show()
mouse = Image.open("/Users/sabitamanandhar/Desktop/pythonProject/working with images with python/mouse.jpg")
print(mouse)
mouse.show()
# showing the size of the image and resizing it again
print(mouse.size)
resize_mouse = (mouse.resize((1500, 1500)))
resize_mouse.show()

# cropping the image
x = 0
y = 0
width = 200
height = 200
crop_mouse = mouse.crop((x, y, width, height))
crop_mouse.show()

# saving the image
crop_mouse.save("/Users/sabitamanandhar/Desktop/pythonProject/working with images with python/cropmouse.Jpeg")

# Pasting an image in the image
paste_mouse = crop_mouse.paste(im = mouse, box = (0, 0))
# paste_mouse.show()

# rotating the image
# 90 degrees
rotate_mouse = mouse.rotate(90)
rotate_mouse.show()

# Flipping and image vertically/horizontally
flipleft = mouse.transpose(PIL.Image.FLIP_LEFT_RIGHT)
flipleft.show()

fliphorizontal = mouse.transpose(PIL.Image.FLIP_TOP_BOTTOM)
fliphorizontal.show()






